# STEM_Semantic Tool for Energy data Management

## Table of Contents
<!-- - [Background](#background) -->
- [Project Structure](#project-structure)
- [Usage](#usage)

## Project Structure
<!-- ![Frame](Pictures/Frame.svg "Frame")
![code](Pictures/code.svg "code") -->

- [LSP-Data und MATRYCS-Data-Model](#data-reading)
- [Data Preprocessing](#data-preprocessing)
- [Matcher](#matcher)
    - [String based methods](#string-based-methods)
    - [Konwledge based methods](#konwledge-based-methods)
    - [Corpus based methods](#corpus-based-methods)
- [Matcher Select](#matcher-select)
- [Matching](#matching)

### Data Reading
#### LSP-Data und MATRYCS-Data-Model
STEM requires 2 inputs in order for successful data model matching:
* data model in folder "MATRYCS Data"
* put raw data  (.xlsx) in folder "Main_read/Read_Data"

The all data being read is saved in 'json' file format for easy viewing and fast loading. In the next steps, Pre-stored json data can be loaded directly.
- MATRYCS_Data.json
```
{
    "Building": {
        "BuildingOperation": {
            "Entity Attributes": {
                "0": "address",
                "1": "alternateName",
                "2": "refRelatedDeviceOperation",
                ...
            }
        ...
        }
    }
    ...
}
```
- Data.json
```
{
    "EPC": {
        "EPC": {
            "0": "CPE",
            "1": "Data Fim",
            "2": "Data In\u00edcio"
        }
    }
}
```


### Data Preprocessing
- Except the normal word preprocessing
    data cleansing. tokenize, stemming, symspell
- there are also some different measures for each semanic similarity methods


### Matcher
Calculate senmatic similarity of the two df_file(one from the matrycs data model, another is the singe lsp file) and apply the different methods. 
Some different methods for the similarity bewteen two phrases. The function **_** of every 'matcher.py' file is used to calculate the similarity between two phrases. And the function **sim** is as interface to calculate the entire similarity between two dataset.

#### String based methods
- editdistance

- jaccard

#### Konwledge based methods

- word_net

- wiki_cons
#### Corpus based methods

- word2vec

- glove

- fasttext

- Bert-base-uncased

- Sbert

### Matcher Select
#### Select
You can select the matcher you want to use in the 
following list of matchers. 

- edit_distance
- wiki_cons
- sbert
- fasttext-300
- ALmatcher
- word_net_pathsim
- jaccard
- Google_encoder
- bert_energy_tsdae_STS
- semantic_sim_sbert_energy
- Old_sentence_Bert
- word2vec
- bert-base-uncased
- sentence_bert_energy_tsdae_STS
- glove-300
- Energy_Bert
- scibert
- bert_Energy_tsdae

####  Attention:
- Please make sure you enter the characters exactly as in the list.
- When the matcher you enter is not in the list, 
the 'jaccard' matcher will be defaulted.

### Matching
#### Schema mapping process overview
![Schema mapping process overview](./IMG/overview.jpg)

#### Dataset level matching
- The dataset-level matching process produces the matching of pairs between datasets
and entities. The goal is to find the matching entity for each dataset and to reduce the
computation effort of semantic-similarity calculation.
- Set different top-k values, and compare different methods based on different k values.
- You can get two results:
```
dlm_jaccard_top3_entities.json

{
    "EPC": [
        "EPC",
        "Device",
        "BuildingOperation"
    ]
}
```
```
dlm_jaccard_top3_pairs_score.json

{
    "('EPC', 'EPC')": 0.625,
    "('EPC', 'Device')": 0.452,
    "('EPC', 'BuildingOperation')": 0.439
}
```
#### Attribute level matching

- Compare different methods based on the result of dataset-level matching.
- You can get two results:
```
alm_3_topjaccard_entities.json

"('EPC', 'EPC')": {
        "Entity Attributes": {
            "0": "id",
            "1": "buildingInfo",
            "2": "certificateCode",
            "3": "certificateObject",
            "4": "climaticZone",
            "5": "CO2Emission",
            "6": "CO2EmissionLabel",
            "7": "coolingDemand",
            "8": "coolingDemandLabel",
            "9": "currentUseThermal",
            "10": "dateOfExpiration",
            "11": "dateOfInspection",
            "12": "dateOfIssue",
            "13": "energyConsumption",
            "14": "energyExported",
            "15": "energyPerformance",
            "16": "energyPerformanceReference",
            "17": "heatingDemandLabel",
            "18": "heatingDegreeDays",
            "19": "note",
            "20": "primaryEnergyConsumption",
            "21": "primaryConsumptionLabel",
            "22": "recommendations",
            "23": "scope",
            "24": "softwareReference"
        },
```
```
alm_top3_jaccard_pairs_score.json    
   
{
    "('EPC', 'EPC')": 0.625
}
```
## Usage
### Model
* How to add a new Model?
  - Download the model you need and put it in the Model folder.
  - Change the file path in Mather.py
  - For example, to add the Energy_Bert model, you need to modify the relevant paths in the following locations:
  ```
  class Energy_Bert(object):
    def __init__(self, model_name=r'.\Models\Bert base uncased_energybert'):
        '''
        Energy_Bert:
        bert-base-uncased after training with Energy related articles
        '''

        self.dp = Data_preprocessing()
        self.tokenizer = BertTokenizer(r'F:\HIWI\vocab\ReplacedVocab.txt')
        self.model = AutoModel.from_pretrained(model_name, output_hidden_states=True)
  ```
  
### Dataset
* Add your raw data in Main_read/Read_Data
* and your raw data must be .xlsx

### Main
* Run 'main.py'.
* Enter the name of matcher, you need to enter 2 name. One of the matcher is for dataset level matching. Another one is for attribute level matching.
  ```
  input_dl_matcher_name = input("Please select dataset level matcher: ")
  input_al_matcher_name = input("Please select attribute level matcher: ")
  ```
* Then you can get results of dataset level matching and attribute level matching, results save in Main_Save
 
# Used in FastApi
* The docker image of the project can be build with the following command :
```
docker build -t stem_1 .
```
* To run the image, use the following command:
```
docker run -d --name stem -p 8002:8002 stem_1
```
* The backend docs will be at  http://localhost:8002/docs

![fastapi](./IMG/fastapi.jpg)
  - Upload
    - You can upload file (.xlsx)
  - Read file
    - You can read different file contents by entering Dataset or DataModel. 
  - Matcher list
    - You can get the matcher name with this function.
  - Get result
    - You can get the final matching result by entering Dataset/Attribute and matcher name.
      * When you enter Dataset und matcher name
    ![dataset](./IMG/get_result_dataset.jpg)
        You can get result
    ![dataset result](./IMG/result_dataset.jpg)
      * When you enter Attribute und matcher name
    ![attribute](./IMG/get_result_attribute.jpg)
        You can get result
    ![attribute result](./IMG/result_attribue.jpg)
  - Similarity calculation
    - Enter two words
    - Enter the name of the matcher you want to choose.
    
    - You can get the similarity between two vocabularies.
    * For example:
    ![similarity calculation](./IMG/sc.jpg)
    Result:
    ![similarity calculation](./IMG/scr.jpg)
  - Search result
    * You can check whether the file has been uploaded by entering the file name. For example: EPC.xlsx
    * You can view the matching results by entering "{upload_file_name}: dataset matching result: {matcher_name}" or "{upload_file_name}: attribute matching result: {matcher_name}".
      * For example: EPC.xlsx: dataset matching result: jaccard
    * You can check the similarity between two words by entering: "similarity: {word_1}: {word_2}: {matcher}"
     