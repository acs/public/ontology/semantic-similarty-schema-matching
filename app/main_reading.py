import pandas as pd
import json
import os


class ReadDataModel(object):

    def read_matrycs_data(self, folder_path):

        data_model = {}
        current_dir = os.getcwd()
        fpath = os.path.join(current_dir, folder_path)
        categories = pd.read_excel(fpath, sheet_name=None)
        for c_name in categories:
            df_c = pd.read_excel(fpath, sheet_name=c_name)
            df_c = pd.DataFrame(df_c[['Entity', 'Entity Attributes', 'Attribute description']])
            df_c.dropna(axis='index', how='all', inplace=True)
            df_c['Entity'].fillna(axis=0, method='ffill', inplace=True)
            # reset the index of df_c from 0 beginning
            df_c = df_c.reset_index(drop=True)
            # how many entities in a category
            entities = []
            for i in range(len(df_c.index)):
                e_name = df_c.loc[i, 'Entity']
                entities.append(e_name)
            entities = list({}.fromkeys(entities).keys())
            # each entity of a category as a dataset
            category = {}
            for e_name in entities:
                df_e = []
                for i in range(len(df_c.index)):
                    if df_c.loc[i, 'Entity'] == e_name:
                        df_e.append(df_c.loc[i, ['Entity Attributes', 'Attribute description']])
                df_e = pd.DataFrame(df_e)
                df_e = df_e.reset_index(drop=True)
                df_e.columns = ['Entity Attributes', 'Attribute description']
                category[e_name] = df_e
            data_model[c_name] = category
        return data_model

    def save_matrycs_file(self, file, save_path):
        file_json = {}
        for c_name, entities in file.items():
            category = {}
            for e_name, df_e in entities.items():
                e = df_e.to_json()
                e = json.loads(e)
                category[e_name] = e
            file_json[c_name] = category
        with open(save_path, "w") as f:
            f.write(json.dumps(file_json, indent=4))

    def matrix_entities(self, save_path):
        data_model_entities = {}
        data_model_descriptions = {}
        loader = DataLoader()
        matrix_data = loader.load_data(save_path)
        for entities in matrix_data.values():
            for e_name, df_entity in entities.items():
                temp_attris = pd.DataFrame(df_entity['Entity Attributes'])
                temp_attris.columns = ['Entity Attributes']
                data_model_entities[e_name] = temp_attris
                temp_descriptions = pd.DataFrame(df_entity['Attribute description'])
                temp_descriptions.columns = ['Attribute description']
                data_model_descriptions[e_name] = temp_descriptions
        return data_model_entities, data_model_descriptions


class ReadExcelData(object):

    def read_excel_files(self, folder_path):
        files = {}

        for root, dirs, file_names in os.walk(folder_path):
            for file_name in file_names:
                file_path = os.path.join(root, file_name)
                file_name_without_ext = os.path.splitext(file_name)[0]
                key_name = file_name_without_ext
                df_list = []

                if file_name.endswith('.xlsx'):
                    try:
                        file = pd.read_excel(file_path)
                    except Exception as e:
                        print(f"Error reading file '{file_name}': {str(e)}")
                        continue

                    file.dropna(axis='index', how='all', inplace=True)
                    list_columns = list(file)
                    df_temp = pd.DataFrame(list_columns)
                    df_temp.columns = [file_name_without_ext]
                    df_list.append(df_temp)

                if key_name in files:
                    files[key_name].append(pd.concat(df_list, axis=1, join='outer'))
                else:
                    files[key_name] = pd.concat(df_list, axis=1, join='outer')

        return files

    def save_excel_data(self, file, save_path):
        file_json = {}
        for c_name, entities in file.items():
            category = {}
            for e_name, df_e in entities.items():
                e = df_e.to_json()
                e = json.loads(e)
                category[e_name] = e
            file_json[c_name] = category
        with open(save_path, "w") as f:
            f.write(json.dumps(file_json, indent=4))


class DataLoader(object):
    def load_data(self, save_path):
        load_path = save_path
        with open(load_path, 'r') as f:
            file_data = json.load(f)

        data_model = {}
        for c_name, entities in file_data.items():
            category = {}
            for e_name, e in entities.items():
                df_e = {}
                for i, j in e.items():
                    temp = {}
                    for k in j.keys():
                        temp[eval(k)] = j.get(k)
                    df_e[i] = temp
                df_e = pd.DataFrame(df_e)
                category[e_name] = df_e
            data_model[c_name] = category
        return data_model

    def load_lsp_dataset(self, save_path):
        load_path = save_path
        fpath = open(load_path, 'r')
        file_data = json.load(fpath)
        file_dict = {}
        for name, file in file_data.items():
            df_temp = {}
            for i, j in file.items():
                temp = {}
                for k in j.keys():
                    temp[eval(k)] = j.get(k)
                df_temp[i] = temp
            df_file = pd.DataFrame(df_temp)
            file_dict[name] = df_file
        return file_dict
