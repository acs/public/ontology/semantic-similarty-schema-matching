import redis
import logging
from fastapi import FastAPI,UploadFile, File
import os
import uvicorn

app = FastAPI()
file_name = None

redis_db = redis.StrictRedis(
    host='localhost',
    port=6379,
    decode_responses=True
)


redis_db.set('foo','bar')
redis_db.get('foo')
value = "123"
a = "hallo"
b = "hello"
key_a = f"{a}"
key_b = f"{b}"
redis_db.set(key_a,value)
print(redis_db.get(key_a))

@app.get("/v1/similarity/{word_1}/{word_2}/{matcher}")
async def similarity_calculation(word_1: str, word_2: str, matcher: str):
    a = word_1
    b = word_2
    value = "123"
    key = f"{a}:{b}:{matcher}"
    redis_db.set(key,value)
    return {"Matcher": matcher, "Similarity": value}


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)