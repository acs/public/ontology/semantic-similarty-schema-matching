import os
import json
from .main_reading import ReadDataModel, ReadExcelData
from .main_matching import DatasetsGeneration, DatasetLevelMatching, AttributeLevelMatching
from .main_matcher_select import MatcherSelect
from fastapi import APIRouter, UploadFile, File, HTTPException, BackgroundTasks
import redis
import os

router = APIRouter()
upload_file_name = None
upload_file_path = r'/stem/app/Main_Read/Read_Data'
data_folder_path = r'/stem/app/Main_Read'
save_folder_path = r'/stem/app/Main_Save'
save_data_name = "Data.json"
data_save_path = os.path.join(data_folder_path, save_data_name)
data_model_path = r'/stem/app/MATRYCS Data/MATRYCS_data_model_v12.xlsx'
save_dm_name = "MATRYCS_Data.json"
model_save_path = os.path.join(data_folder_path, save_dm_name)

m = MatcherSelect()
matcher_list = m.matcher_list()

attribute_level_matcher = None
dataset_level_matcher = None


REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", 6379)

try:
    redis_db = redis.StrictRedis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        password=None,
        decode_responses=True
    )
except Exception as e:
    raise HTTPException(status_code=500, detail=f"Failed to connect to Redis: {str(e)}")

@router.post("/v1/file")
async def upload_file(file: UploadFile = File(...)):
    try:
        contents = await file.read()
        save_path = os.path.join(upload_file_path, file.filename)
        redis_key = file.filename
        global upload_file_name
        upload_file_name = file.filename

        with open(save_path, "wb") as f:
            f.write(contents)

        value = "The following files have been uploaded: " + str(file.filename)

        redis_db.set(redis_key,value )
        return "The following files have been uploaded: " + str(file.filename)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"File upload failed: {str(e)}")



@router.get("/v1/file/{item}")

async def read_json_file(item: str):
    # read upload file
    read_excel_path = upload_file_path
    rd = ReadExcelData()
    files = rd.read_excel_files(read_excel_path)
    rd.save_excel_data(files, data_save_path)

    # read Data_model
    dm = ReadDataModel()
    read_data_model = dm.read_matrycs_data(data_model_path)
    dm.save_matrycs_file(read_data_model, model_save_path)

    # read file in FastAPI
    json_file_path = {
        "Dataset": "/stem/app/Main_Read/Data.json",
        "DataModel": "/stem/app/Main_Read/MATRYCS_Data.json"}

    if item in json_file_path:
        file_path = json_file_path[item]
        with open(file_path, "r") as file:
            json_data = json.load(file)
        return json_data
    else:
        return {"error": "Item not found"}



@router.get("/v1/matcher/matcher_list")
async def show_matcher_list():
    return {"matcher": matcher_list}

@router.get("/v1/result/{item}/{matcher}")
async def get_result(item: str, matcher: str):
    # select Matcher
    if matcher in matcher_list:
        matcher_name = matcher
    else:
        matcher_name = 'jaccard'

    global upload_file_name


    # create entities
    dg = DatasetsGeneration(data_save_path, model_save_path)
    lsp_datasets = dg.lsp_datasets()
    dme, dmd = dg.matrycs_entities()
    save_dl_scores_path = r'/stem/app/Main_Save/dataset_level/dlm_{0}_top{1}_pairs_score.json'
    save_dl_entities_path = r'/stem/app/Main_Save/dataset_level/dlm_{0}_top{1}_entities.json'
    save_atl_scores_path = r'/stem/app/Main_Save/attribute_level/alm_top{0}_{1}_pairs_score.json'
    save_atl_mapping_path = r'/stem/app/Main_Save/attribute_level/alm_{0}_top{1}_entities.json'

    # DatasetLevelMatching
    # k=3

    dlm = DatasetLevelMatching()
    # matcher_dl_name = get_dl_matcher_name()
    top_entities, top_pairs_score = dlm.dataset_top(
        datasets=lsp_datasets,
        entities=dme,
        k=3,
        method=matcher_name,
        save_path_scores=save_dl_scores_path,
        save_path_entities=save_dl_entities_path
    )

    # AttributeLevelMatching

    alm = AttributeLevelMatching()

    # matcher_al_name = select_al_matcher()
    pairs_mapping, pairs_score = alm.dataset2entity(
        datasets=lsp_datasets,
        entities=dme,
        top_entities=top_entities,
        k=3,
        method=matcher_name,
        save_path_mapping=save_atl_mapping_path,
        save_path_score=save_atl_scores_path
    )

    json_file_path = {
        "Dataset": "/stem/app/Main_Save/dataset_level",
        "Attribute": "/stem/app/Main_Save/attribute_level"
    }


    json_files = []
    if item in json_file_path:
        file_dir = json_file_path[item]
        for file_name in os.listdir(file_dir):
            if file_name.endswith(".json"):
                file_path = os.path.join(file_dir, file_name)
                with open(file_path, "r") as file:
                    json_data = json.load(file)
                    json_file = {
                        "file_name": file_name,
                        "data": json_data
                    }
                    json_files.append(json_file)
        if item == "Dataset":
            redis_key = f"{upload_file_name}: dataset matching result: {matcher_name}"
        if item == "Attribute":
            redis_key = f"{upload_file_name}: attribute matching result: {matcher_name}"
        redis_value = json.dumps({"File": upload_file_name, "Matcher": matcher_name, "Result": json_files})
        redis_db.set(redis_key, redis_value)
        return {"Matcher": matcher_name, "Result": json_files}
    else:
        return {"error": "Item not found"}
@router.get("/v1/similarity/{word_1}/{word_2}/{matcher}")
async def similarity_calculation(word_1: str, word_2: str, matcher: str):
    a = [word_1]
    b = [word_2]
    if matcher in matcher_list:
        matcher = matcher
    else:
        matcher = 'jaccard'

    matcher_select = m.matcher_select(matcher)
    result = matcher_select.sim(a, b)
    score = result.item()

    redis_key = f"similarity: {word_1}: {word_2}: {matcher}"
    redis_value = json.dumps({"Similarity": score})
    redis_db.set(redis_key, redis_value)

    return {"Matcher": matcher, "Similarity": score}

@router.get("/v1/result_redis/{key}")
async def get_result_from_redis(key: str):
    return redis_db.get(key)

