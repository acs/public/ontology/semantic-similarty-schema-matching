from app.routers import router
from fastapi import FastAPI

# uvicorn main_api:app --reload --host 127.0.0.1 --port 8010
app = FastAPI()

app.include_router(router)


