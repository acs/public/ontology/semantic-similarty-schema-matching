from .Matcher.String_based_methods.string_based_methods import Edit_distance, Jaccard
from .Matcher.Konwledge_based_methods.word_net import Word_net
from .Matcher.Konwledge_based_methods.wiki_cons import Wiki_cons
from .Matcher.Corpus_based_methods.wordembedding import Word2vec, Glove, Fast_text, Bert, Sbert, Energy_Bert, \
    Google_encoder, bert_Energy_tsdae, bert_energy_tsdae_STS, sentence_bert_energy_tsdae_STS, scibert, \
    semantic_sim_sbert_energy, Old_sentence_Bert

import json


class MatcherSelect(object):

    def matcher_select(self, matcher_select):

        matcher = None
        if matcher_select == 'edit_distance':
            matcher = Edit_distance()
        elif matcher_select == 'jaccard':
            matcher = Jaccard()
        elif matcher_select == 'word_net_pathsim':
            matcher = Word_net(art='path_sim')

        elif matcher_select == 'wiki_cons':
            matcher = Wiki_cons(art='con_sim4')
        elif matcher_select == 'word2vec':
            matcher = Word2vec()
        elif matcher_select == 'glove-300':
            matcher = Glove(model_name='glove-wiki-gigaword-300')
        elif matcher_select == 'fasttext-300':
            matcher = Fast_text()
        # result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'bert-base-uncased':
            matcher = Bert(model_name='bert-base-uncased')
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'sbert':
            matcher = Sbert(model_name='sentence-transformers/all-MiniLM-L6-v2')
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'Energy_Bert':
            matcher = Energy_Bert()
        # result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'Google_encoder':
            matcher = Google_encoder()
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'bert_Energy_tsdae':
            matcher = bert_Energy_tsdae()
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'bert_energy_tsdae_STS':
            matcher = bert_energy_tsdae_STS()
            # result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'sentence_bert_energy_tsdae_STS':
            matcher = sentence_bert_energy_tsdae_STS()
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'scibert':
            matcher = scibert()
        #  result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'semantic_sim_sbert_energy':
            matcher = semantic_sim_sbert_energy()
        # result = matcher.sim(dataset_phase, entity_phase)
        elif matcher_select == 'Old_sentence_Bert':
            matcher = Old_sentence_Bert()
        # result = matcher.sim(dataset_phase, entity_phase)
        return matcher

    def matcher_scores(self, matcher_select, excel_data, data_model):
        matcher = self.matcher_select(matcher_select)
        scores = matcher.sim(excel_data, data_model)
        return scores

    def matcher_list(self):
        matcher_set = {
            'edit_distance', 'jaccard', 'word_net_pathsim',
            'wiki_cons', 'word2vec', 'glove-300', 'fasttext-300',
            'bert-base-uncased', 'sbert', 'ALmatcher', 'Energy_Bert',
            'Google_encoder', 'bert_Energy_tsdae', 'bert_energy_tsdae_STS',
            'sentence_bert_energy_tsdae_STS', 'scibert', 'semantic_sim_sbert_energy',
            'Old_sentence_Bert'
        }
        matcher_list = list(matcher_set)
        for i in range(0, len(matcher_list),5):
            row = matcher_list[i:i+5]
            print(row)
        return matcher_list

    def save_matcher_result(self, matcher_name, result, save_path):

        tensor_list = result.tolist()
        data = {matcher_name: tensor_list}

        with open(save_path, "w") as f:
            f.write(json.dumps(data, indent=4))
