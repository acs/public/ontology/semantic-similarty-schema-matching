import json
import os
import uvicorn
from fastapi import FastAPI, UploadFile, File


app = FastAPI()


class RunApi:
    def run(self, host: str = "0.0.0.0", port: int = 8000):
        uvicorn.run(app, host=host, port=port)


class UploadFiles:

    def __init__(self, files_directory):
        self.setup_routes()
        self.fd = files_directory

    # save uploaded file in Directory
    async def process_uploaded_file(self, file: UploadFile):
        contents = await file.read()
        save_path = os.path.join(self.fd, file.filename)
        with open(save_path, "wb") as f:
            f.write(contents)
        return "The following files has been uploaded:" + str(file.filename)

    def setup_routes(self):
        @app.post("/upload")
        async def upload_file(file: UploadFile = File(...)):
            return await self.process_uploaded_file(file)


class ReadFile:

    def read_json(self, json_file_path):
        @app.get("/read_json/{item}")
        async def read_json_file(item: str):
            if item in json_file_path:
                file_path = json_file_path[item]
                with open(file_path, "r") as file:
                    json_data = json.load(file)

                return json_data
            else:
                return {"error": "Item not found"}

class SelectMatcher:
    def select_matcher(self):
        @app.get("select_matcher")
        async def select_matcher():
            return 0

class GetResult:

    def get_result(self,json_file_path):
        @app.get("/get_result/{item}")
        async def read_json_file(item: str):
            json_files = []
            if item in json_file_path:
                file_dir = json_file_path[item]
                for file_name in os.listdir(file_dir):
                    if file_name.endswith(".json"):
                        file_path = os.path.join(file_dir,file_name)
                        with open(file_path, "r") as file:
                            json_data = json.load(file)
                            json_file = {
                                "file_name": file_name,
                                "data": json_data
                            }
                            json_files.append(json_file)

            return json_files



if __name__ == '__main__':
    # upload file from api
    # upload file in http://127.0.0.1:8000/docs
    file_directory = '../Main_Read/Read_Data'
    uf = UploadFiles(file_directory)
    # read json file
    json_files_path = {
        "Dataset": "../Main_Read/Data.json",
        "DataModel": "../Main_Read/MATRYCS_Data.json"
    }
    rj = ReadFile()
    rj.read_json(json_files_path)

    result_file_path = {
        "Dataset": "../Main_Save/dataset_level",
        "Attribute": "../Main_Save/attribute_level"
    }
    gr = GetResult()
    gr.get_result(result_file_path)

    run = RunApi()
    run.run("127.0.0.1", 8000)
