import logging
import os
import aiofiles
from fastapi import HTTPException, UploadFile

log = logging.getLogger("uvicorn")

async def save_file(file:UploadFile,filestore:str)->str:
    try:
        async with aiofiles.open(os.path.join(filestore,file.filename),"wb")as f:
            for i in iter(lambda:file.file.read(1024*1024),b""):
                await f.write(i)
        log.info(f"File saved as {file.filename}")
    except Exception as e:
        problem_response = {"type": str(type(e).__name__), "details": str(e)}
        headers = {"Content-Type": "application/problem+json"}
        log.error(problem_response)
        raise HTTPException(status_code=500, detail=problem_response, headers=headers)
    return file.filename


