FROM python:3.8

WORKDIR /stem
COPY ./requirements.txt /stem/requirements.txt
RUN pip install -r requirements.txt
RUN python -c "import nltk; nltk.download('punkt')"
RUN python -c "import nltk; nltk.download('averaged_perceptron_tagger')"
RUN python -c "import nltk; nltk.download('wordnet')"
COPY ./app /stem/app
EXPOSE 8002
CMD ["uvicorn", "app.main_api:app", "--host", "0.0.0.0", "--port", "8002"]
# "--reload",
# docker run -d --name stem -p 8002:8002 stem_1