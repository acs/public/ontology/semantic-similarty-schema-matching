
import sys
import os
sys.path.append((r"."))
from app.main_reading import ReadDataModel, ReadExcelData
from app.main_matching import DatasetsGeneration, DatasetLevelMatching, AttributeLevelMatching
from app.main_matcher_select import MatcherSelect


if __name__ == '__main__':
    data_folder_path = r'app/Main_Read'
    save_folder_path = r'app/Main_Save'
    read_excel_path = os.path.join(data_folder_path, 'Read_Data')



    rd = ReadExcelData()
    files = rd.read_excel_files(read_excel_path)
    save_data_name = "Data.json"
    data_save_path = os.path.join(data_folder_path, save_data_name)
    save_files = rd.save_excel_data(files, data_save_path)

    # read Data_model
    data_model_path = r'app/MATRYCS Data/MATRYCS_data_model_v12.xlsx'
    dm = ReadDataModel()
    save_dm_name = "MATRYCS_Data.json"
    model_save_path = os.path.join(data_folder_path, save_dm_name)
    read_data_model = dm.read_matrycs_data(data_model_path)
    dm.save_matrycs_file(read_data_model, model_save_path)


    # create entities
    dg = DatasetsGeneration(data_save_path, model_save_path)
    lsp_datasets = dg.lsp_datasets()
    dme, dmd = dg.matrycs_entities()

    # select matcher
    # if no input, set defaut
    m = MatcherSelect()
    matcher_list = m.matcher_list()

    input_dl_matcher_name = input("Please select dataset level matcher: ")
    input_al_matcher_name = input("Please select attribute level matcher: ")
    if input_dl_matcher_name in matcher_list:
        matcher_dl_name = input_dl_matcher_name
    else:
        matcher_dl_name = 'jaccard'

    if input_al_matcher_name in matcher_list:
        matcher_al_name = input_al_matcher_name
    else:
        matcher_al_name = 'jaccard'

    # DatasetLevelMatching
    # k=3
    save_dl_scores_path = r'app/Main_Save\dataset_level\dlm_{0}_top{1}_pairs_score.json'
    save_dl_entities_path = r'app/Main_Save\dataset_level\dlm_{0}_top{1}_entities.json'
    dlm = DatasetLevelMatching()
    top_entities, top_pairs_score = dlm.dataset_top(
        datasets=lsp_datasets,
        entities=dme,
        k=3,
        method=matcher_dl_name,
        save_path_scores=save_dl_scores_path,
        save_path_entities=save_dl_entities_path
    )

    # AttributeLevelMatching
    save_atl_scores_path = r'app/Main_Save\attribute_level\alm_top{0}_{1}_pairs_score.json'
    save_atl_mapping_path = r'app/Main_Save\attribute_level\alm_{0}_top{1}_entities.json'

    alm = AttributeLevelMatching()
    pairs_mapping, pairs_score = alm.dataset2entity(
        datasets=lsp_datasets,
        entities=dme,
        top_entities=top_entities,
        k=3,
        method=matcher_al_name,
        save_path_mapping=save_atl_mapping_path,
        save_path_score=save_atl_scores_path
    )


